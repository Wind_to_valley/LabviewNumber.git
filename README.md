::: keywords
labview,OCR,灰度,对比度,数字识别
:::

# 前言

OCR （Optical Character
Recognition，光学字符识别）是指电子设备（例如扫描仪或数码相机）检查纸上打印的字符，通过检测暗、亮的模式确定其形状，然后用字符识别方法将形状翻译成计算机文字的过程。

本文基于labview设计了一个学号识别系统，可以识别学生卡上学号并保存在文件中，同时也可以识别身份证号、名片电话等。

该系统包含图像获取、图像处理、数据输出，是一个综合性的设计，作为学习labview的阶段性练习。

# 运行环境

本系统运行在labview2020或2018环境，需要安装labview的视觉组件。

1.  [LabVIEW软件](https://www.ni.com/zh-cn/support/downloads/software-products/download.labview.html#369643)

2.  [Vision Development
    Module（视觉开发模块）](https://www.ni.com/zh-cn/support/downloads/software-products/download.vision-development-module.html#367068)

3.  [NI Vision Acquisition
    Software（视觉采集软件）](https://www.ni.com/zh-cn/support/downloads/drivers/download.vision-acquisition-software.html#367318)

[安装过程](https://handsome-man.blog.csdn.net/article/details/115579676)

# 设计原理

## 图像格式

1.  **彩色图像**：由RGB三原色数值排列表示一个像素，有RGB888（24位）和RGB565（16位）等格式。其中，24位全彩色图像中的每个像素占据3个byte的空间，分别表示RGB3个通道，总共可以显示$256^3$种颜色

2.  **灰度图**：由灰度信息表示一个像素（8位，0-255：0表示最暗色，255表示最亮色），每个像素存放在一个byte空间

3.  **二值图像**：由二值信息表示一个像素，每个下昂苏存放在一个bit空间

## OCR数字识别

OCR（光学字符识别）是指机器自动从图像中识别文本字符的过程，是目标分类和识别的一种应用，包括训练与分类过程，可用于对被测件的识别和分类。

OCR训练过程主要从图像中提取用于字符识别的特征向量，并对各字符图像赋予准确的字符值。具有相同字符值的字符样本图像构成一个字符类，该类可以用其中一个最能代表该类字符的样本图像来代表，称为参考字符。字符训练完成后，就可得到一个用于对字符进行识别的字符集。

OCR对图像中的文本进行读取时，会先将图像中的各个字符图像分割开来，并将字符的特征向量与字符集中保存的特征向量进行对比，选取满足条件的最佳匹配向量所对应的字符值作为读取识别结果。若有必要，也可以通过字符验证过程对OCR的识别质量进行验证。

常见的OCR识别应用包括：药品包装标签识别、IC芯片编码读取、冲压零件上的字符识别、汽车零件编码读取以及车牌识别等。

利用Labview的Vision模块的OCR组件可以实现从训练到识别的过程。

## 图像处理

机器视觉系统开发过程中常见的特征包括像素灰度、边缘、轮廓和形状、纹理、角点、色彩以及各种与图像颗粒相关的属性等。

本文进行OCR识别的对象为学生卡或其他证件，一般有明显的数字特征，字体之间有明确间隔，字体背景为浅颜色，故获取彩色图像后，需要对其灰度化、增强亮度和对比度处理。这些函数都可以在labview的vision组件中找到

## 文件存储

利用labview的文件I/O组件可以实现将字符串换行写入txt文件中，实现OCR识别结果的存储。

# 程序设计

## 程序流程

![程序图](https://img-blog.csdnimg.cn/2401d54ed328429cb76672fee1e5a521.png)

![程序框图](https://img-blog.csdnimg.cn/9d31374d06884f36828d939858f8b3eb.png)


![最终效果](https://img-blog.csdnimg.cn/68f7c413fd4d40ab98675c9549e1db8d.png)


## 程序步骤

### 相机获取

![开启摄像头](https://img-blog.csdnimg.cn/e0d6d438eb064094b1f568b2391fd3ad.png)

1.  利用**IMAQdx Open Camera
    VI**开启摄像头，查询相机功能，加载相机配置文件，输入摄像头名称(cam0为笔记本自带摄像头)，输出相机引用。

2.  利用**IMAQ Create VI**为图像创建临时内存位置。

3.  摄像头引用节点输入**IMAQdx Configure Grab
    VI**，配置并从缓冲区抓取图像。

4.  创建while循环，将图像和引用输入**IMAQdx Grab2
    VI**，输出最新的帧获取到Image Out，并通过Image显示。

### 图像处理

![图像处理](https://img-blog.csdnimg.cn/6fea329167b645cc9990066594f73da5.png)·

1.  将摄像头实时获取的彩色图像输入**IMAQ ExtractSingleColorPlane
    VI**，输出单通道灰度图。

2.  将单通道灰度图输入**IMAQ BCGLookup VI**
    ，创建亮度、对比度和gamma矫正输入，调节典型值(80,80,1)，获得图像增强后的输出。

3.  当环节内有error输出或按下暂停键，停止图像更新，并将最新一帧增强图像输出。

### OCR训练

OCR的字符集可由Nl OCR训练器应用程序离线训练得到。

1.  打开软件**Vision Assistant 2021 (32-bit)**

2.  利用手机、相机拍摄多张彩色、灰色、增强图像，用OCR训练器打开

    ![OCR训练](https://img-blog.csdnimg.cn/56a206909d744a94bcef05ce91a68d2c.png)




3.  创建训练集，对图片手动添加数据集并标定

    ![手动添加训练](https://img-blog.csdnimg.cn/836ed88eab144998bebfa8d7f8514dd9.png)

    ![不同的数据集](https://img-blog.csdnimg.cn/55da6faed9c6480f8f10f5fcd419e496.png)


4.  保存为文件**studentnumber.abc**，验证模型效果

    ![彩色图识别预览](https://img-blog.csdnimg.cn/c9622a1f868e49409d7f92223e3410e4.png)

### OCR识别

![OCR识别程序](https://img-blog.csdnimg.cn/68b0b01dae1f450f856b7c609bc2b865.png)

1.  利用**IMAQ OCR Create Session VI** ，创建OCR会话，返回句柄。

2.  利用**IMAQ OCR Read Character Set File
    VI**读取字符集**studentnumber.abc**，添加到读取过程使用的训练字符集。

3.  Labview的Image可以创建ROI属性节点，框选区域，输出矩形对角坐标。创建鼠标释放事件，当框选结束时，执行对框选区域的OCR识别。

4.  利用**IMAQ Clear Overlay VI**，清除图像覆盖，保证识别帧为最新一帧。

5.  利用**IMAQ OCR Read Text 3
    VI**，利用训练字符集读取ROI框选图像中的文本，将每个对象与字符集的每个字符比较，选择与对象最匹配的字符，输出为字符串。

### 文件保存

![文件存储](https://img-blog.csdnimg.cn/c2b562c9380f4fa7a3021008381e415b.png)

1.  读取当前路径和文件名，利用**创建路径（函数）**生成路径。

2.  利用**打开/创建/替换文件 (函数)** ，打开路径对应的**Save.txt文件**。

3.  利用**设置文件位置 (函数)** ，设置写入位置为文件末尾。

4.  利用**连接字符串 (函数)** ，在输出的字符串前添加回车符。

5.  利用**写入文本文件 (函数)** ，将识别结果写入txt文件。

![识别效果](https://img-blog.csdnimg.cn/8df486c3062b445ca5358177d6ddf918.png)

至此，OCR数字识别程序已完成

# 实验结果

经测试，对学生卡等白背景数字识别准确率较高，而对银行卡等带颜色背景数字识别有误差。

![识别统计](https://img-blog.csdnimg.cn/0239ac76fb504eb5a3ed229a0ea8c253.png)

对于长串数字，若光照和颜色均匀，有较为准确的识别结果。

![识别效果](https://img-blog.csdnimg.cn/366826f72c674c4eb59faad85a1ef64c.png)

数据集的完整性对识别结果有明显影响，采集数据时未能找到学号带有数字6的学生卡，故数据集中数字6只有一个，在对银行卡号中的数字6，有高概率误认为数字8.

# 下载
关注公众号**小电动车**，回复**数字识别**获取Labview工程文件

![请添加图片描述](https://img-blog.csdnimg.cn/765b65508bf942d29a0da9cb883c7af8.png)

